local wezterm = require 'wezterm'

local config = {}

if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.color_scheme = 'Monokai Pro (Gogh)'
config.font = wezterm.font 'JetBrains Mono'
config.audible_bell = "Disabled"

return config
