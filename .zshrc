setopt interactivecomments
# enables comments like this one

source ~/dotfiles/shared.sh

# autocomplete
autoload -U compinit; compinit

# move cursor with SHIFT + arrows
bindkey ";2D" backward-word
bindkey ";2C" forward-word

alias rc="cat ~/.zshrc"

# this function writes the first or second last executed command to the zshrc and optionally comments on top with whatever you type afterwards
function rc1() { if [ $# -ne 0 ]; then echo "\n# $@" >> ~/.zshrc; fi; fc -ln -1  >> ~/.zshrc; }
function rc2() { if [ $# -ne 0 ]; then echo "\n# $@" >> ~/.zshrc; fi; fc -ln -2 | head -n 1 >> ~/.zshrc; }

function precmd () {
  echo # add new line after each command
  echo -ne "\033]0;$(pwd | sed -e "s;^$HOME;~;")\a" # rename the window to current directory
}

PROMPT='%(?.%F{green}●.%F{red}●) %F{white}%3~ %(!.#.>) '
RPROMPT='%F{8} %*%f'

# log shell history to file
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=1000

# enable sharing history, but not immediately between all terminals
setopt INC_APPEND_HISTORY_TIME