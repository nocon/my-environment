#!/bin/bash

APT_GET_CMD=$(which apt-get 2>/dev/null)
BREW_CMD=$(which brew)
DOTFILES_DIR=$(pwd)

function update {
    if [[ ! -z $APT_GET_CMD ]]; then
        apt-get update
    elif [[ ! -z $BREW_CMD ]]; then
        brew update
        brew tap homebrew/cask-versions
    else
        echo "Couldn't find any supported package manager."
    fi
}

function install {
    if [[ ! -z $APT_GET_CMD ]]; then
        apt-get install "$@"
    elif [[ ! -z $BREW_CMD ]]; then
        brew install "$@"
    fi
}

function install-app {
    if [[ ! -z $BREW_CMD ]]; then
        brew cask install "$@"
    fi
}

function pin {
    if [[ ! -z $BREW_CMD ]]; then
        defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/$@.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>";
        killall Dock;
    fi
}